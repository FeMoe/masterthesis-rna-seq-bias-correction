import pickle
import h5py


def read_file(file_path):
    pkl_file = open(file_path, 'rb')
    file_content = pickle.load(pkl_file)
    pkl_file.close()
    return file_content


def save_file(input_struct, output_name):
    '''
    gets a structure and saves it in file
    '''
    output_file = open(output_name + '.pkl', 'wb')
    pickle.dump(input_struct, output_file)
    output_file.close()


def read_counting_tables(h5py_file):
    '''
    gets hdf5-file containing context-counting-tables
    returns them as numpy-arrays
    '''
    hdf5file = h5py.File(h5py_file, 'r')
    counting_table_genes = hdf5file['KmerCountsGenes'].value
    counting_table_reads = hdf5file['StartKmersReads'].value
    hdf5file.close()
    return counting_table_genes, counting_table_reads


def process_context_probabilities(counting_table_reads):
    '''
    gets startcontext-counting-table
    processes context probabilites
    returns dict of context-names and probabilites
    '''
    kmer_probabilites = dict()
    _, kmers = counting_table_reads.shape
    total_kmers_counts = counting_table_reads.sum()
    for kmer in range(kmers):
        kmer_probabilites[kmer] = counting_table_reads[:, kmer].sum()/total_kmers_counts
    return kmer_probabilites

def transform_gene_regions(gene_regions):
    '''
    gets a dict with gene-name as key and a triple chromosome-name, gene-start and gene-end as item
    creates a dict with chromosome-name as key and gene-name, gene-start and gene-end as item
    returns created dict
    '''
    transformed_gene_regions = dict()
    for gene, (chromosome, exon_regions) in gene_regions.items():
        if chromosome not in transformed_gene_regions:
            transformed_gene_regions[chromosome] = [(gene, exon_regions)]
        else:
            transformed_gene_regions[chromosome].append((gene, exon_regions))
    return transformed_gene_regions