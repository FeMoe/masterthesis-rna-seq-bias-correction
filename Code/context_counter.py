import argparse
import cProfile
import numpy as np
from itertools import product
from common_functions import save_file, read_file, transform_gene_regions
import h5py


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    gene_regions = read_file(args.gene_regions)
    read_information = args.read_information
    chromosome_sequences = read_file(args.chromosome_sequences)
    gene_sequences = get_gene_sequences(gene_regions, chromosome_sequences)
    gene_regions = transform_gene_regions(gene_regions)
    gene_indexes = read_file(args.gene_indexes)
    kmer_length = args.kmer_length
    rev_compl_kmers = create_complementary_kmers(kmer_length)
    kmer_indexes = create_kmer_dictionary(kmer_length)
    save_file(kmer_indexes, args.output+'_'+str(kmer_length)+'_context_idx')
    counting_table_genes = count_kmers_on_genes(gene_sequences, gene_indexes, kmer_indexes, rev_compl_kmers)
    del gene_sequences
    counting_table_reads = count_startkmers_on_reads(read_information, gene_indexes, kmer_indexes, kmer_length)
    save_hdf5(counting_table_genes, counting_table_reads, args.output+'_'+str(kmer_length))



def save_hdf5(kmer_count_genes, kmer_count_reads, hdf5path):
    '''
    Gets countext counts of genes and reads as numpy-arrays and saves them to hdf5-file
    '''
    hdf5file = hdf5path.split('.')[0]
    f = h5py.File(hdf5file + '_context_tables.hdf5', 'w')
    f.create_dataset("KmerCountsGenes", data=kmer_count_genes)
    f.create_dataset("StartKmersReads", data=kmer_count_reads)
    f.close()


def count_startkmers_on_reads(read_information, gene_indexes, kmer_indexes, kmer_length):
    '''
    gets sam-file, creates counting table for each kmer and gene
    counts startkmers of each read
    returns counting table
    '''
    counting_table_reads = np.zeros((len(gene_indexes), len(kmer_indexes)), dtype='float64')
    corrupted_sequences = 0
    to_short_matches = 0
    total_reads = 0
    with open(read_information, 'r') as reads:
        for read in reads:
            total_reads += 1
            read = read.split()
            matches = int(read[2])
            if matches >= kmer_length:
                kmer_sequence = read[1][:kmer_length]
                if not 'N' in kmer_sequence:
                    gene_ids = read[0].split(';')
                    for gene_id in gene_ids:
                        gene_index = gene_indexes[gene_id]
                        kmer_index = kmer_indexes[kmer_sequence]
                        counting_table_reads[gene_index, kmer_index] += 1
                else:
                    corrupted_sequences += 1
            else:
                to_short_matches += 1
    print('Startmatches to short: '+str(to_short_matches))
    print('Corrupted Sequences: '+str(corrupted_sequences))
    print('Total reads:'+str(total_reads))
    return counting_table_reads


def count_kmers_on_genes(gene_sequences, gene_indexes, kmer_indexes, rev_compl_kmers):
    '''
    gets gene-sequences, creates counting table for each kmer and gene
    counts kmers on every gene
    return counting table
    '''
    counting_table_genes = np.zeros((len(gene_indexes), len(kmer_indexes)), dtype='float64')
    kmers = rev_compl_kmers.keys()
    for gene in gene_sequences.keys():
        gene_sequence = gene_sequences[gene]
        gene_idx = gene_indexes[gene]
        for kmer in kmers:
            kmer_idx = kmer_indexes[kmer]
            sequence_pos = 0
            kmer_count = 0
            while True:
                sequence_pos = gene_sequence.find(kmer, sequence_pos) + 1
                if sequence_pos > 0:
                    kmer_count += 1
                else:
                    break
            counting_table_genes[gene_idx, kmer_idx] += kmer_count
            rev_kmer = rev_compl_kmers[kmer]
            kmer_idx = kmer_indexes[rev_kmer]
            counting_table_genes[gene_idx, kmer_idx] += kmer_count
    return counting_table_genes


def get_gene_sequences(gene_regions, chromosome_sequences):
    gene_sequences = dict()
    for gene_id, (gene_chr, exon_regions) in gene_regions.items():
        gene_sequence = ''
        chr_sequence = chromosome_sequences[gene_chr]
        for exon_start, exon_end in exon_regions:
            gene_sequence += chr_sequence[exon_start-1:exon_end]
        gene_sequences[gene_id] = gene_sequence
    return gene_sequences


def create_kmer_dictionary(kmer_length):
    '''
    creates a dictonary containing all kmers over A, C, G, T and an corresponding index
    returns dictonary
    '''
    kmer_index = 0
    kmer_indexes = dict()
    kmers = [''.join(x) for x in product('ACGT', repeat=kmer_length)]
    for kmer in kmers:
        kmer_indexes[kmer] = kmer_index
        kmer_index += 1
    return kmer_indexes


def create_complementary_kmers(kmer_length):
    '''
    returns dictonary containing all kmers over A, C, G, T and its reverse complements
    '''
    complementary_base = dict()
    complementary_base['A'] = 'T'
    complementary_base['C'] = 'G'
    complementary_base['G'] = 'C'
    complementary_base['T'] = 'A'
    kmers = [''.join(x) for x in product('ACGT', repeat=kmer_length)]
    rev_compl_kmers = dict()
    rev_compl_kmer = None
    for kmer in kmers:
        reverse_kmer = kmer[::-1]
        rev_compl_kmer = ''
        for character in reverse_kmer:
            rev_compl_kmer += complementary_base[character]
        rev_compl_kmers[kmer] = rev_compl_kmer
    return rev_compl_kmers


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('read_information', help='file containg readnumber, mapped genes and length of starting matching positions')
    parser.add_argument('chromosome_sequences', help='file containing dictonary with chromosome-ids and sequences')
    parser.add_argument('gene_regions', help='File containg chromosome and exon-regions for each gene')
    parser.add_argument('gene_indexes', help='file containg gene-ids and their indexes')
    parser.add_argument('kmer_length', help='length of k-mers which should be counted. max length is 10', type=int)
    parser.add_argument('output', help='save both kmer counting tables as hdf5-file. Filename-Extension will always be *.hdf5', type=str)
    return parser


if __name__ == "__main__":
    main()
    #cProfile.run("main()")
