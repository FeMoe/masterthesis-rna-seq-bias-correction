import argparse
import pickle
from common_functions import read_file
import matplotlib.pyplot as plt
from collections import Counter
from operator import itemgetter
from itertools import product
import h5py


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    start_pos_fwd = read_file(args.start_pos_fwd)
    start_pos_rev = read_file(args.start_pos_rev)
    gene_ids = read_file(args.gtest_ids)
    gene_regions = read_file(args.gene_regions)
    chromosome_sequences = read_file(args.chromosome_sequences)
    coverage_threshold = args.coverage_threshold
    min_coverage = args.min_coverage
    kmer_length = args.k_mer
    highest_kmers = dict()
    total_processed_positions = 0
    highest_kmers, processed_positions = get_highest_kmers(start_pos_fwd, gene_ids, gene_regions, chromosome_sequences, coverage_threshold, min_coverage, highest_kmers, kmer_length)
    total_processed_positions += processed_positions
    rev_compl_kmers = create_complementary_kmers(kmer_length)
    highest_kmers, processed_positions = get_highest_kmers(start_pos_rev, gene_ids, gene_regions, chromosome_sequences, coverage_threshold, min_coverage, highest_kmers, kmer_length, rev_compl_kmers)
    total_processed_positions += processed_positions
    processed_information = process_information(highest_kmers)
    print_processed_information(processed_information)
    print('Total processed positions: '+str(total_processed_positions))
    if args.coverage_graph is not None:
        coverage_graph(start_pos_fwd, start_pos_rev, args.coverage_graph)
    if args.missing_hexamers:
        counting_table_genes, counting_table_reads = read_counting_tables(args.missing_hexamers[0])
        gene_to_idx = read_file(args.missing_hexamers[1])
        missing_hexamers = get_missing_hexamers(counting_table_genes, counting_table_reads, gene_ids, gene_to_idx)
        for hexamer, missing in missing_hexamers.items():
            if missing:
                print('Hexamer never at readstart: '+str(hexamer))


def get_missing_hexamers(counting_table_genes, counting_table_reads, gene_ids, gene_to_idx):
    '''
    Gets arrays of hexamer-counts on genes and reads and a list of gene-ids
    returns dict of hexamers which are existing on genes of list but have no reads
    '''
    missing_hexamers = dict()
    for gene_id in gene_ids:
        gene_idx = gene_to_idx[gene_id]
        for i in range(counting_table_reads.shape[1]):
            if counting_table_reads[gene_idx][i] == 0:
                if counting_table_genes[gene_idx][i] != 0 and i not in missing_hexamers:
                    missing_hexamers[i] = True
            else:
                missing_hexamers[i] = False
    return missing_hexamers


def read_counting_tables(h5py_file):
    '''
    gets filepath of hdf5-file
    reads file and returns numpy-arrays from file
    '''
    hdf5file = h5py.File(h5py_file, 'r')
    counting_table_genes = hdf5file['HexamerCountsGenes'].value
    counting_table_reads = hdf5file['StartHexamersReads'].value
    hdf5file.close()
    return counting_table_genes, counting_table_reads


def print_processed_information(processed_information):
    '''
    gets list of kmers containing triple of kmer, reads starting with kmer and number of startpositions
    prints all three values for each kmer
    '''
    processed_information = sorted(processed_information, key=itemgetter(2))
    for kmer, total_reads, total_start_pos in processed_information:
        average_reads_per_pos = total_reads/total_start_pos
        #print(kmer+' & '+str(total_start_pos)+' & '+str(average_reads_per_pos)+' \\ ')
        print('Context: '+kmer+'\tTotal Reads: '+str(total_reads)+'\tStart Positions: '+str(total_start_pos)+'\tAverage: '+str(average_reads_per_pos))


def process_information(highest_kmers):
    '''
    gets dict of kmers and their occurence on gene
    returns list of hexamer, its number of startpositions and corresponding number of reads
    '''
    processed_information = []
    for kmer, coverages in highest_kmers.items():
        total_reads = sum(coverages)
        total_start_pos = len(coverages)
        processed_information.append((kmer, total_reads, total_start_pos))
    return processed_information


def get_highest_kmers(start_positions, gene_ids, gene_regions, chromosome_sequences, threshold, min_coverage, highest_kmers, kmer_length, rev_compl_kmers=None):
    '''
    gets list of gene-ids, minimum coverage and threshold
    counts on every gene with at least one position with more that >minimum coverage< reads the occurence of each kmer above the threshold
    returns dict with list of read coverages per kmer
    '''
    total_processed_positions = 0
    for gene_id in gene_ids:
        start_pos_on_gene = start_positions[gene_id]
        pos_coverage = Counter(start_pos_on_gene)
        highest_coverage = max(pos_coverage.values())
        if highest_coverage >= min_coverage:
            coverage_threshold = (threshold)*highest_coverage
            for position, coverage in pos_coverage.items():
                if coverage >= coverage_threshold:
                    total_processed_positions += 1
                    chr_name = gene_regions[gene_id][0]
                    chromosome_sequence = chromosome_sequences[chr_name]
                    if rev_compl_kmers is None:
                        kmer = chromosome_sequence[position - 1:position + kmer_length - 1]
                    else:
                        kmer = chromosome_sequence[position - kmer_length:position]
                        kmer = rev_compl_kmers[kmer]
                    if kmer not in highest_kmers:
                        highest_kmers[kmer] = [coverage]
                    else:
                        highest_kmers[kmer].append(coverage)
    return highest_kmers, total_processed_positions


def coverage_graph(forward_startpositions, reverse_startpositions, gene_id):
    '''
    gets lists of read startpositions and draws histograms showing read startposition coverage
    '''
    gene_forward_positions = forward_startpositions[gene_id]
    gene_reverse_positions = reverse_startpositions[gene_id]
    min_forward = min(gene_forward_positions)
    max_forward = max(gene_forward_positions)
    nbr_bins_f = max_forward - min_forward + 1
    fig1 = plt.figure()
    hist_forw = fig1.add_subplot(111)
    hist_forw.hist(
        gene_forward_positions, bins=nbr_bins_f)
    fig1.savefig(gene_id + '_forward.pdf')
    min_reverse = min(gene_reverse_positions)
    max_reverse = max(gene_reverse_positions)
    nbr_bins_r = max_reverse - min_reverse + 1
    fig2 = plt.figure()
    hist_rev = fig2.add_subplot(111)
    hist_rev.hist(
        gene_reverse_positions, bins=nbr_bins_r)
    fig2.savefig(gene_id + '_reverse.pdf')


def create_complementary_kmers(kmer_length):
    '''
    returns dictonary containing all hexamers over A, C, G, T and its reverse complements
    '''
    complementary_base = dict()
    complementary_base['A'] = 'T'
    complementary_base['C'] = 'G'
    complementary_base['G'] = 'C'
    complementary_base['T'] = 'A'
    hexamers = [''.join(x) for x in product('ACGT', repeat=kmer_length)]
    rev_compl_hexamers = dict()
    rev_compl_hexamer = None
    for hexamer in hexamers:
        reverse_hexamer = hexamer[::-1]
        rev_compl_hexamer = ''
        for character in reverse_hexamer:
            rev_compl_hexamer += complementary_base[character]
        rev_compl_hexamers[hexamer] = rev_compl_hexamer
    return rev_compl_hexamers


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'start_pos_fwd', help='Table with forward read start positions')
    parser.add_argument(
        'start_pos_rev', help='Table with reverse read start positions')
    parser.add_argument('coverage_threshold', help='Only startcoverage positions above threshold get considered. Range 0.0 - 1.0', type=float)
    parser.add_argument('min_coverage', help='Only genes with at least one position which has a coverage above minimum value are being considered', type=int)
    parser.add_argument('gene_regions', help='File containg chromosome and exon-regions for each gene')
    parser.add_argument('chromosome_sequences', help='pkl-file containing dict of chromosome sequences', type=str)
    parser.add_argument('gtest_ids', help='pkl-File containing list of gene-ids which shall be processed')
    parser.add_argument('k_mer', help='k-mer length which should be counted', type=int)
    parser.add_argument('-cg', '--coverage_graph',
                        help='Draws histogram of forward and reverse read starts', type=str, default=None)
    parser.add_argument('-mh', '--missing_hexamers', help='Prints hexamers existings on genes but not on readstarts. 1st argument: hexamer-tables, 2nd argument: gene indices', nargs=2)
    
    return parser


if __name__ == "__main__":
    main()
