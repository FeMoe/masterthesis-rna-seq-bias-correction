import argparse
from common_functions import read_counting_tables, read_file
import matplotlib.pyplot as plt
import math


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    _, counting_table = read_counting_tables(args.context_tables)
    content_distribution = process_content_distribution(counting_table)
    draw_content_distribution(content_distribution, args.output)



def draw_content_distribution(content_distribution, output):
    total_content = len(content_distribution)
    plt.plot(list(range(total_content)), content_distribution)
    #plt.plot(list(range(total_content)), [0]*total_content)
    plt.xlabel('Kontexte')
    plt.ylabel('log-Wahrscheinlichkeit')
    plt.savefig(output+'probability_distribution.pdf')


def process_content_distribution(counting_table):
    '''
    processed log to base 2 context-distribution taking uniform distribution into account
    '''
    total_countings = counting_table.sum()
    content_distribution = []
    _, content_number = counting_table.shape
    for content in range(content_number):
        content_probability = counting_table[:, content].sum()/total_countings
        content_distribution.append((math.log(content_probability*content_number, 2)))
    content_distribution = sorted(content_distribution)
    return content_distribution


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('context_tables', help='h5py-file containing context counting tables')
    parser.add_argument('output', help='name of outputfile')
    return parser

if __name__ == "__main__":
    main()
