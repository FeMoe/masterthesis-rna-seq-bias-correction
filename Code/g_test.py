import argparse
import cProfile
import math
import matplotlib.pyplot as plt
from common_functions import read_file, save_file, read_counting_tables
import numpy as np


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    counting_table_genes, counting_table_reads = read_counting_tables(args.context_tables)
    gene_indexes = read_file(args.gene_idx)
    idx_to_gene = {gene_idx: gene for gene, gene_idx in gene_indexes.items()}
    normalize_counted_kmers(counting_table_genes)
    g_values_dict, idx_worst_gene, genes_in_range = make_gtest(counting_table_genes, counting_table_reads, args.min_reads, args.grange)
    g_values = list(g_values_dict.values())
    if args.grange:
        gene_ids = []
        for gene, g_value in genes_in_range:
            gene_id = idx_to_gene[gene]
            gene_ids.append(gene_id)
            print('__________________')
            print('Gene ID: '+str(gene_id))
            print('G-Value: '+str(g_value))
        save_file(gene_ids, args.output+'_gtest_ids')
    print('---------------')
    print('Gene with highest g-value: '+idx_to_gene[idx_worst_gene])
    print('#genes with at least '+str(args.min_reads)+' mapped reads: '+str(len(g_values)))
    draw_histogram(g_values, args.output)


def normalize_counted_kmers(kmer_count_genes):
    '''
    gets numpy-array of kmer counts on genes and normalizes them by total kmer counts on gene
    '''
    nbr_genes = kmer_count_genes.shape[0]
    nbr_kmers = kmer_count_genes.shape[1]
    for gene in range(nbr_genes):
        kmers_on_gene = kmer_count_genes[gene].sum()
        for kmer in range(nbr_kmers):
            if kmers_on_gene != 0:
                kmer_count_genes[gene, kmer] = kmer_count_genes[gene, kmer]/kmers_on_gene


def make_gtest(kmer_count_genes, kmer_count_reads, genethreshold, g_range):
    '''
    gets numpy-arrays of kmer counts on gene and read-startKamers and a gene threshold
    return list of g-values and gene with highest g-value
    '''
    g_values = dict()
    skipped_genes = 0
    worst_gvalue = 0
    worst_gene = None
    kmer_not_on_gene = 0
    genes_in_range = []
    if g_range is not None:
        range_min, range_max = g_range
    else:
        range_min = 0
        range_max = 0
    for gene in range(len(kmer_count_genes)):
        total_kmers = kmer_count_reads[gene].sum()
        if total_kmers >= genethreshold:
            g_value = 0
            for kmer in range(len(kmer_count_genes[gene])):
                kmer_occurence_reads = kmer_count_reads[gene][kmer]
                kmer_occurence_gene = kmer_count_genes[gene][kmer]
                if kmer_occurence_reads == 0:
                    pass
                elif total_kmers == 0 or kmer_occurence_gene == 0:
                    g_value = float('inf')
                    if kmer_occurence_gene == 0:
                        kmer_not_on_gene += 1
                    break
                else:
                    g_value += kmer_occurence_reads*math.log(kmer_occurence_reads/(total_kmers*kmer_occurence_gene))
            if g_value != float('inf'):
                g_value = 2*g_value
                if g_value > worst_gvalue:
                    worst_gene = gene
                    worst_gvalue = g_value
                g_values[gene] = g_value
                if range_min <= g_value <= range_max:
                    genes_in_range.append((gene, g_value))
            else:
                skipped_genes += 1
    print('K-mer not on gene: '+str(kmer_not_on_gene))
    print('Skipped genes: '+str(skipped_genes))
    print('Worst gvalue: '+str(worst_gvalue))
    return g_values, worst_gene, genes_in_range


def draw_histogram(g_values, filepath):
    '''
    gets list of g-values and creates histogram
    '''
    plt.figure()
    value_weights = np.ones_like(g_values)/len(g_values)
    plt.hist(g_values, 100, weights=value_weights, histtype='bar', color=['crimson'], range=(0, 30000))
    plt.xlabel('G-Wert')
    plt.ylabel('Anteil Gene')
    filepath = filepath.split('.')[0]
    plt.savefig(filepath+'_g_values.pdf')


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'context_tables', help='h5py-file containing context counting tables')
    parser.add_argument(
        'context_idx', help='file containing context-indexes')
    parser.add_argument(
        'gene_idx', help='file containing gene-indexes')
    parser.add_argument('min_reads', help='min number of reads that have to be mapped to gene', type=int)
    parser.add_argument('output', help='name of outputfile. Gets extended by gene_ids.pkl', type=str)
    parser.add_argument('-gr', '--grange', help='creates pkl-file containing Gene-IDs of genes with g-values in given range', nargs=2, type=int)
    return parser


if __name__ == "__main__":
    main()
    #cProfile.run("main()")