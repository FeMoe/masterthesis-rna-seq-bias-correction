import argparse
from common_functions import read_file, save_file, read_counting_tables, process_context_probabilities
import matplotlib.pyplot as plt
import numpy as np


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    max_genelength = args.max_gene_length
    gene_indexes = read_file(args.gene_indexes)
    inv_gene_indexes = {v: k for k, v in gene_indexes.items()}
    counting_table_genes, counting_table_reads = read_counting_tables(args.context_tables)
    gene_affinites_plot, gene_lengths, gene_affinities_output, gene_lengths_output = process_gene_affinity(counting_table_genes, counting_table_reads, inv_gene_indexes, max_genelength)
    save_file(gene_affinities_output, args.output+'_gene_affinites')
    save_file(gene_lengths_output, args.output+'_gene_lengths')
    if args.slope or args.ma_plot:
        gene_affinites_plot, gene_lengths = transform_with_slope(gene_affinites_plot, gene_lengths)
    if args.ma_plot:
        transform_to_ma(gene_affinites_plot, gene_lengths)
    draw_content_distribution(gene_affinites_plot, gene_lengths, args.output)


def transform_to_ma(gene_probabilities, gene_lengths):
    '''
    Scales x- and y-axis for plotting a MA-Plot
    '''
    for gene in range(len(gene_probabilities)):
        gene_probability = gene_probabilities[gene]
        gene_length = gene_lengths[gene]
        gene_probabilities[gene] = gene_probability-gene_length
        gene_lengths[gene] = (gene_probability+gene_length)/2
    return gene_probabilities, gene_lengths


def draw_content_distribution(gene_probabilities, gene_lengths, filepath):
    '''
    Draws plot with gene_probabilities on y-axis and gene_length on x-axis
    '''
    plt.plot(gene_lengths, gene_probabilities, 'r.')
    plt.ylabel('Differenz zwischen skalierter Affinität und Genlänge')
    plt.xlabel('Mittelwert von skalierte Affinität und Genlänge')
    plt.ylim(-5000, 4000)
    plt.savefig(filepath+'_gene_affinities.png', dpi=600)


def transform_with_slope(gene_probabilities, gene_lengths):
    '''
    Scales y-axis by normalizing with median slope
    '''
    slopes = []
    for gene in range(len(gene_probabilities)):
        slopes.append(gene_probabilities[gene]/gene_lengths[gene])
    slope = np.median(np.array(slopes))
    for gene in range(len(gene_probabilities)):
        gene_probabilities[gene] /= slope
    return gene_probabilities, gene_lengths


def process_gene_affinity(counting_table_genes, counting_table_reads, gene_indexes, max_genelength):
    '''
    processes probability for reads is mapped to gene by concidering gene-length and kmer frequencies on gene
    '''
    gene_affinites = dict()
    gene_lengths = dict()
    content_probabilites = process_context_probabilities(counting_table_reads)
    gene_affinities_plot = []
    gene_lengths_plot = []
    genes, context_number = counting_table_reads.shape
    for gene in range(genes):
        gene_length = counting_table_genes[gene].sum()
        gene_affinity = 0
        for context in range(context_number):
            gene_affinity += counting_table_genes[gene, context]*content_probabilites[context]
        gene_affinites[gene_indexes[gene]] = gene_affinity
        gene_lengths[gene_indexes[gene]] = gene_length
        if gene_length <= max_genelength:
            gene_affinities_plot.append(gene_affinity)
            gene_lengths_plot.append(gene_length)
    return gene_affinities_plot, gene_lengths_plot, gene_affinites, gene_lengths


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('context_tables', help='h5py-file containing context-counting-tables')
    parser.add_argument('gene_indexes', help='file containing gene-indexes')
    parser.add_argument('output', help='prefix of output-file', type=str)
    parser.add_argument('-gl', '--max_gene_length', help='only genes with equal or less gen-length are taken into account', type=int, default=float('Inf'))
    parser.add_argument('-s', '--slope', help='takes slope into account', action='store_true')
    parser.add_argument('-ma', '--ma_plot', help='draws a MA-Plot', action='store_true')
    return parser

if __name__ == "__main__":
    main()
