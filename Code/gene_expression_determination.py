import argparse
from common_functions import read_file, save_file
import numpy as np
import h5py


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    read_count_files = args.raw_data
    gene_indexes = read_file(args.gene_indexes)
    gene_affinities = read_file(args.gene_affinities)
    gene_lengths = read_file(args.gene_lengths)
    raw_counts, sample_indexes = process_raw_counts(
        read_count_files, gene_indexes)
    affinity_normalized_counts = affinity_normalization(
        raw_counts, gene_indexes, gene_affinities)
    length_normalized_counts = length_normalization(raw_counts, gene_indexes, gene_lengths)
    save_file(sample_indexes, args.output+'_sample_idx')
    save_counts(raw_counts, affinity_normalized_counts, length_normalized_counts, args.output)


def save_counts(raw_counts, affinity_normalized_counts, length_normalized_counts, output):
    f = h5py.File(output + '_sample_read_counts.hdf5', 'w')
    f.create_dataset("RawCounts", data=raw_counts)
    f.create_dataset("LengthNormalizedCounts", data=length_normalized_counts)
    f.create_dataset("AffinityNormalizedCounts", data=affinity_normalized_counts)
    f.close()


def affinity_normalization(raw_counts, gene_indexes, gene_affinities):
    '''
    gets numpy-array with read counts, a dict with gene-affinites and a dict containing gene-indexes
    returns numpy-array containing geneexpressionvalue for each gene of each file in raw_counts
    '''
    genes, samples = raw_counts.shape
    affinity_normalized_counts = np.zeros((genes, samples), dtype='float64')
    for gene in gene_indexes.keys():
        for sample in range(samples):
            affinity_normalized_counts[gene_indexes[gene], sample] = raw_counts[gene_indexes[gene], sample]/gene_affinities[gene]
    return affinity_normalized_counts


def length_normalization(raw_counts, gene_indexes, gene_lengths):
    '''
    gets numpy-array with read counts, a dict with gene-lengths and a dict containing gene-indexes
    returns numpy-array containing geneexpressionvalue for each gene of each file in raw_counts
    '''
    genes, samples = raw_counts.shape
    length_normalized_counts = np.zeros((genes, samples), dtype='float64')
    for gene in gene_indexes.keys():
        for sample in range(samples):
            length_normalized_counts[gene_indexes[gene], sample] = raw_counts[gene_indexes[gene], sample]/gene_lengths[gene]
    return length_normalized_counts


def process_raw_counts(file_list, gene_indexes):
    '''
    gets list of files
    parses every file and saves annotated genes and reads to numpy-array
    returns numpy-array
    '''
    raw_counts = np.zeros((len(gene_indexes), len(file_list)), dtype='float64')
    sample_indexes = dict()
    sample_index = 0
    for sample_file in file_list:
        sample_name = sample_file.split('.')[0].split('/')[-1]
        sample_indexes[sample_name] = sample_index
        with open(sample_file, 'r') as sample_content:
            for line in sample_content:
                line = line.split()
                gene_id = line[0]
                if gene_id in gene_indexes:
                    raw_counts[gene_indexes[gene_id], sample_index] = float(
                        line[1])
        sample_index += 1
    return raw_counts, sample_indexes


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'raw_data', help='One or more files containing gene-ids and number of reads on gene', nargs='+')
    parser.add_argument('gene_indexes', help='file containing gene-indexes')
    parser.add_argument('gene_affinities', help='file containing preprocessed gene-affinites for each gene')
    parser.add_argument('gene_lengths', help='file containing preprocessed gene-lengths for each gene')
    parser.add_argument('output', help='name of outputfile')
    return parser


if __name__ == "__main__":
    main()
