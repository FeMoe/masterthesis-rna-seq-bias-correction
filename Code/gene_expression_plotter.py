import argparse
from common_functions import read_file
import matplotlib.pyplot as plt
import h5py
import numpy as np
import math


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    sample_idx = read_file(args.sample_indexes)
    first_sample_idx = sample_idx[args.first_sample_name]
    second_sample_idx = sample_idx[args.second_sample_name]
    length_normalized_ev, affinity_normalized_ev = read_sample_count(args.sample_read_counts)
    sample1_length = length_normalized_ev.T[first_sample_idx]
    sample2_length = length_normalized_ev.T[second_sample_idx]
    sample1_length, sample2_length = normalize_axis(sample1_length, sample2_length)
    sample1_affinity = affinity_normalized_ev.T[first_sample_idx]
    sample2_affinity = affinity_normalized_ev.T[second_sample_idx]
    sample1_affinity, sample2_affinity = normalize_axis(sample1_affinity, sample2_affinity)
    samples_affinity = transform_to_ma(sample1_affinity, sample2_affinity)
    mean_values_affinity, average_values_affinity, upper_deviation_affinity, lower_deviation_affinity = process_expectancy_values(samples_affinity)
    plot_samples(samples_affinity, mean_values_affinity, average_values_affinity, upper_deviation_affinity, lower_deviation_affinity, 'r.', args.output+'_affinity')
    sample1_length = normalize_expression_values(sample1_affinity, sample1_length)
    sample2_length = normalize_expression_values(sample2_affinity, sample2_length)
    samples_length = transform_to_ma(sample1_length, sample2_length)
    mean_values_length, average_values_length, upper_deviation_length, lower_deviation_length = process_expectancy_values(samples_length)
    plot_samples(samples_length, mean_values_length, average_values_length, upper_deviation_length, lower_deviation_length, 'b.', args.output+'_length')
    plot_lines(mean_values_affinity, average_values_affinity, upper_deviation_affinity, lower_deviation_affinity, mean_values_length, average_values_length, upper_deviation_length, lower_deviation_length, args.output)


def plot_lines(mean_values_affinity, average_values_affinity, upper_deviation_affinity, lower_deviation_affinity, mean_values_length, average_values_length, upper_deviation_length, lower_deviation_length, output):
    plt.clf()
    plt.plot
    plt.plot(average_values_affinity, mean_values_affinity, 'r')
    affinity, = plt.plot(average_values_affinity, upper_deviation_affinity, 'r')
    plt.plot(average_values_affinity, lower_deviation_affinity, 'r')
    length, = plt.plot(average_values_length, mean_values_length, 'b')
    plt.plot(average_values_length, upper_deviation_length, 'b')
    plt.plot(average_values_length, lower_deviation_length, 'b')
    plt.xlabel('Durchschnitt Genexpressionswert')
    plt.ylabel('Differenz Genexpressionswerte')
    plt.legend([affinity, length], ['Affinitätsnormalisiert', 'Längennormalisiert'])
    plt.savefig(output+'_lines.png', dpi=600)


def process_expectancy_values(ma_data):
    upper_deviation = []
    lower_deviation = []
    expectancy_values = []
    average_values = []
    averages, differences = zip(*ma_data)
    for i in range(len(averages)):
        gene_count = 0
        difference = 0
        average_value = averages[i]
        if average_value not in expectancy_values:
            temp = []
            average_values.append(average_value)
            for j in range(len(averages)):
                if average_value-2 <= averages[j] <= average_value+2:
                    temp.append(differences[j])
                    difference += differences[j]
                    gene_count += 1
            mean_value = difference/gene_count
            expectancy_values.append(mean_value)
            deviation = 0
            temp = np.array(temp)
            for value in np.nditer(temp):
                deviation += (value-mean_value)**2
            deviation *= (1/(len(temp)-1))
            deviation = math.sqrt(deviation)
            upper_deviation.append(deviation+mean_value)
            lower_deviation.append(mean_value-deviation)
    return expectancy_values, average_values, upper_deviation, lower_deviation


def normalize_axis(expression_values_sample1, expression_values_sample2):
    slopes = []
    for gen in range(len(expression_values_sample1)):
        if expression_values_sample1[gen] != 0:
            slopes.append(expression_values_sample2[gen]/expression_values_sample1[gen])
    slope = np.median(np.array(slopes))
    for gen in range(len(expression_values_sample2)):
        expression_values_sample2[gen] /= slope
    return expression_values_sample1, expression_values_sample2


def normalize_expression_values(affinity_sample, length_sample):
    ratios = []
    for gen in range(len(affinity_sample)):
        if length_sample[gen] != 0 or affinity_sample[gen] != 0:
            ratios.append(affinity_sample[gen]/length_sample[gen])
    ratio = np.median(np.array(ratios))
    length_sample *= ratio
    return length_sample


def transform_to_ma(sample1, sample2):
    transformed_samples = []
    for gen in range(len(sample1)):
        if sample1[gen] != 0 and sample2[gen] != 0:
            sample1_value = math.log(sample1[gen], 2)
            sample2_value = math.log(sample2[gen], 2)
            difference = sample2_value-sample1_value
            average = (sample1_value+sample2_value)/2
            transformed_samples.append((average, difference))
    return sorted(transformed_samples, key=lambda tup: tup[0])


def plot_samples(ma_data, mean_values, average_values, std_deviations_upper, std_deviations_lower, color, output):
    plt.clf()
    averages, differences = zip(*ma_data)
    affinity, = plt.plot(averages, differences, color, alpha=0.1)
    meanvalue, = plt.plot(average_values, mean_values, 'g')
    std_deviation, =plt.plot(average_values, std_deviations_lower, 'k')
    plt.plot(average_values, std_deviations_upper, 'k')
    plt.legend([meanvalue, std_deviation], ['Mittelwert', 'Standardabweichung'])
    plt.xlabel('Durchschnitt Genexpressionswert')
    plt.ylabel('Differenz Genexpressionswerte')
    plt.savefig(output+'.png', dpi=600)


def read_sample_count(sample_read_counts):
    hdf5file = h5py.File(sample_read_counts, 'r')
    length_normalized_ev = hdf5file['LengthNormalizedCounts'].value
    affinity_normalized_ev = hdf5file['AffinityNormalizedCounts'].value
    hdf5file.close()
    return length_normalized_ev, affinity_normalized_ev


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('sample_read_counts', help='hdf5-File containing raw-read counts, length- and affinity-normalized genes for each sample')
    parser.add_argument('sample_indexes', help='pkl-file containing index of each sample')
    parser.add_argument('first_sample_name', help='name of first sample')
    parser.add_argument('second_sample_name', help='name of second sample')
    parser.add_argument('output', help='name of outputfile')
    return parser


if __name__ == "__main__":
    main()
