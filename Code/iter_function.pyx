# -*- coding: utf-8 -*-
#!python
#cython: language_level=3


# C imports
cimport cython


@cython.boundscheck(False)
@cython.wraparound(False)
cdef list _get_gene_ids_startpos(str chromosome, int read_start_pos, int read_end_pos, dict gene_regions):
    '''
    gets a dict of genes and their start- and end-regions on a chromosome and the start position of a read
    returns name of the gene on with the read starts
    '''
    cdef list gene_ids = []
    cdef list regions_on_chromosome
    regions_on_chromosome = gene_regions[chromosome]
    cdef str gene_id
    cdef list exon_regions
    cdef int exon_start, exon_end
    for gene_id, exon_regions in regions_on_chromosome:
        start_found = False
        end_found = False
        for exon_start, exon_end in exon_regions:
            if read_start_pos >= exon_start and read_start_pos <= exon_end:
                start_found = True
            if read_end_pos >= exon_start and read_end_pos <= exon_end:
                end_found = True
            if start_found and end_found:
                gene_ids.append(gene_id)
                break
    return gene_ids


@cython.boundscheck(False)
@cython.wraparound(False)
cpdef list get_gene_ids_startpos(str chromosome, int read_start_pos, int read_end_pos, dict gene_regions):
    return _get_gene_ids_startpos(chromosome, read_start_pos, read_end_pos, gene_regions)
