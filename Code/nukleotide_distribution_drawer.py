import argparse
from common_functions import read_file, save_file, transform_gene_regions
from iter_function import get_gene_ids_startpos
import matplotlib.pyplot as plt


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    sam_path = args.sam
    gene_regions = read_file(args.gene_regions)
    gene_regions = transform_gene_regions(gene_regions)
    gene_ids = read_file(args.genes) if args.genes is not None else None
    nbr_bases = args.bases
    base_pos_count = count_bases(sam_path, gene_regions, gene_ids, nbr_bases)
    #save_file(base_pos_count, args.output+'_nukleotid_distribution')
    #base_pos_count = read_file(args.sam)
    print(base_pos_count)
    draw_graph(base_pos_count, args.output)


def draw_graph(base_pos_count, output):
    base_color = {'A': 'r', 'C': 'g', 'G': 'y', 'T': 'b', 'N': 'm'}
    plt.xlabel('Position auf Read')
    plt.ylabel('Basenvorkommen')
    total_nukleotides = [0]*len(base_pos_count['A'])
    for base in base_pos_count.keys():
        for i in range(len(base_pos_count[base])):
            total_nukleotides[i] += base_pos_count[base][i]
    for base, pos_countings in base_pos_count.items():
        positions = list(range(1, len(pos_countings)+1))
        for i in range(len(pos_countings)):
            pos_countings[i] = pos_countings[i]/total_nukleotides[i]
        plt.plot(positions, pos_countings, label=base, color=base_color[base])
    plt.xlim(1, 20)
    plt.legend()
    plt.savefig(output+'_nukleotide_distribution.pdf')


def count_bases(sam_path, gene_regions, observed_gene_ids, nbr_bases):
    base_pairs = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'N': 'N'}
    skipped_reads = 0
    base_pos_count = dict()
    for base in ['A', 'C', 'G', 'T']:
        base_pos_count[base] = [0] * nbr_bases
    with open(sam_path, 'r') as sam_file:
        for line in sam_file:
            line = line.split()
            chromosome_name = line[2]
            if chromosome_name != '*':
                count_bases = False
                flag = line[1]
                start_pos = int(line[3])
                rev_complementary = False
                if int(flag) & 0x10 == 16:
                    rev_complementary = True
                    cigar = line[5]
                    end_pos = get_compl_start_pos(cigar, start_pos)
                read_seq = line[9]
                if rev_complementary:
                    read_seq_compl = read_seq[-nbr_bases:][::-1]
                    read_seq = ''
                    for base in read_seq_compl:
                        read_seq += base_pairs[base]
                else:
                    read_seq = read_seq[:nbr_bases]
                if 'N' in read_seq:
                    skipped_reads += 1
                else:
                    gene_ids = get_gene_ids_startpos(chromosome_name, start_pos, end_pos, gene_regions)
                    if observed_gene_ids is not None:
                        for gene_id in gene_ids:
                            if gene_id in observed_gene_ids:
                                count_bases = True
                                break
                    elif not gene_ids:
                        count_bases = True
                if count_bases:
                    for i in range(nbr_bases):
                        base_pos_count[read_seq[i]][i] += 1
    print('Skipped Reads: '+str(skipped_reads))
    return base_pos_count


def get_compl_start_pos(cigar, read_start_pos):
    '''
    gets startposition of a read and its corresponding cigar-string

    returns most right startposition of read
    '''
    length_operators = ['D', 'M', 'N', 'X', '=']
    ignore_operators = ['I', 'H', 'P']
    total_length = 0
    partial_length = ''
    for char in cigar:
        if char in length_operators:
            total_length += int(partial_length)
            partial_length = ''
        elif char in ignore_operators:
            partial_length = ''
        else:
            partial_length += char
    hexamer_start_pos = read_start_pos + total_length - 1
    return hexamer_start_pos


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('sam', help='Path of sam-file')
    parser.add_argument(
        'gene_regions', help='File containg chromosome and exon-regions for each gene', type=str)
    parser.add_argument('output', help='Name of output graph', type=str)
    parser.add_argument(
        '-g', '--genes', help='List of gene-ids for which nukleotide distribution should be drawn.', default=None)
    parser.add_argument(
        '-b', '--bases', help='Number of bases which should be counted. Default is 20.', type=int, default=20)
    return parser

if __name__ == "__main__":
    main()
