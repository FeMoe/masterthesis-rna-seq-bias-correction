import argparse
from common_functions import save_file
from iter_function import get_gene_ids_startpos
from itertools import product


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    reference = args.referencefile
    sam = args.samfile
    geneanotation = args.geneanotationfile
    rev_compl_decamers = create_complementary_decamers()
    chromosome_sequences = get_chromosome_sequences(reference)
    save_file(chromosome_sequences, args.output + '_chr_seqs')
    gene_regions, gene_indexes = get_gene_information(geneanotation, chromosome_sequences)
    save_file(gene_regions, args.output + '_gene_regions')
    save_file(gene_indexes, args.output + '_gene_idx')
    gene_regions = transform_gene_regions(gene_regions)
    reverse_mapped_reads, forward_mapped_reads = process_reads(sam, chromosome_sequences, rev_compl_decamers, gene_regions, gene_indexes)
    save_file(forward_mapped_reads, args.output+'_fwd_start_pos')
    save_file(reverse_mapped_reads, args.output+'_rev_start_pos')


def get_start_decamer(chromosome_seq, cigar, start_pos, rev_compl_decamers, rev_complementary):
	'''
	gets a chromosome-sequence, a startposition and a cigar-string
	reads substring from chromosome-sequence and returns it
	'''
    read_seq = ''
    current_pos = start_pos
    for (length, operator) in cigar:
        if operator == 'M':
            read_seq += chromosome_seq[current_pos-1:current_pos+length-1]
            current_pos += length
        if operator in ['N', 'D']:
            current_pos += length
    if rev_complementary:
        decamer_seq = read_seq[-10:]
        return rev_compl_decamers[decamer_seq], current_pos-1
    else:
        return read_seq[:10], current_pos-1


def convert_cigar_to_list(cigar):
    cigar_list = []
    length = ''
    operators = ['M', 'N', 'I', 'D']
    for char in cigar:
        if char not in operators:
            length += char
        else:
            cigar_list.append((int(length), char))
            length = ''
    return cigar_list


def create_complementary_decamers():
    '''
    returns dictonary containing all kmers over A, C, G, T and its reverse complements
    '''
    complementary_base = dict()
    complementary_base['A'] = 'T'
    complementary_base['C'] = 'G'
    complementary_base['G'] = 'C'
    complementary_base['T'] = 'A'
    complementary_base['N'] = 'N'
    decamers = [''.join(x) for x in product('ACGTN', repeat=10)]
    rev_compl_decamers = dict()
    for decamer in decamers:
        reverse_decamer = decamer[::-1]
        rev_compl_decamer = ''
        for character in reverse_decamer:
            rev_compl_decamer += complementary_base[character]
        rev_compl_decamers[decamer] = rev_compl_decamer
    return rev_compl_decamers


def process_reads(sampath, chromosome_sequences, rev_compl_decamers, gene_regions, gene_indexes):
	'''
	get a sam-file, a dict of chromosome-sequences and a dict of gene_regions
	parses sam-file and collects relevant read information
	prints read-information and returns dict of forward and reverse restart-positions
	'''
    forward_start_positions = {key: [] for key in gene_indexes.keys()}
    reverse_start_positions = {key: [] for key in gene_indexes.keys()}
    read_number = 0
    with open(sampath, 'r') as samfile:
        for line in samfile:
            read_number += 1
            line = line.split()
            chromosome_name = line[2]
            if chromosome_name != '*':
                flag = line[1]
                start_pos = int(line[3])
                cigar = convert_cigar_to_list(line[5])
                rev_complementary = False
                if int(flag) & 0x10 == 16:
                    rev_complementary = True
                chromosome_sequence = chromosome_sequences[chromosome_name]
                start_decamer, end_pos = get_start_decamer(chromosome_sequence, cigar, start_pos, rev_compl_decamers, rev_complementary)
                gene_ids = get_gene_ids_startpos(chromosome_name, start_pos, end_pos, gene_regions)
                if gene_ids:
                    matches, _ = cigar[-1] if rev_complementary else cigar[0]
                    gene_ids_output = ''
                    for gene_id in gene_ids:
                        gene_ids_output += gene_id+';'
                        if rev_complementary:
                            reverse_start_positions[gene_id].append(end_pos)
                        else:
                            forward_start_positions[gene_id].append(start_pos)
                    print(gene_ids_output[:-1]+'\t'+start_decamer+'\t'+str(matches))
    return reverse_start_positions, forward_start_positions


def transform_gene_regions(gene_regions):
    '''
    gets a dict with gene-name as key and a triple chromosome-name, gene-start and gene-end as item
    creates a dict with chromosome-name as key and gene-name, gene-start and gene-end as item
    returns created dict
    '''
    transformed_gene_regions = dict()
    for gene, (chromosome, exon_regions) in gene_regions.items():
        if chromosome not in transformed_gene_regions:
            transformed_gene_regions[chromosome] = [(gene, exon_regions)]
        else:
            transformed_gene_regions[chromosome].append((gene, exon_regions))
    return transformed_gene_regions


def get_gene_information(geneanotation, chromosome_sequences):
    '''
    gets gene-anotation-file and reference file
    processes gene-anotation-file and saves gene-regions, gene-sequences and gene_indexes as dictionaries
    returns dictionaries
    '''
    gene_idx = 0
    gene_regions = dict()
    gene_indexes = dict()
    with open(geneanotation, 'r') as anotation_file:
        for line in anotation_file:
            line = line.split()
            current_chromosome = line[0]
            if current_chromosome in chromosome_sequences:
                current_gene_id = line[line.index('gene_id') + 1][1:-2]
                current_exon_start = int(line[3])
                current_exon_end = int(line[4])
                if current_gene_id in gene_regions:
                    saved_chromosome, exon_regions = gene_regions[
                        current_gene_id]
                    if saved_chromosome != current_chromosome:
                        print('Error: Gene on different chromosomes!')
                    else:
                        last_exon_start, last_exon_end = exon_regions[-1]
                        if current_exon_start <= last_exon_end and current_exon_end > last_exon_end:
                            exon_regions[-
                                         1] = (last_exon_start, current_exon_end)
                        elif current_exon_start > last_exon_end:
                            exon_regions.append(
                                (current_exon_start, current_exon_end))
                        gene_regions[current_gene_id] = (
                            current_chromosome, exon_regions)
                else:
                    gene_regions[current_gene_id] = (
                        current_chromosome, [(current_exon_start, current_exon_end)])
                    gene_indexes[current_gene_id] = gene_idx
                    gene_idx += 1
    return gene_regions, gene_indexes


def get_chromosome_sequences(reference):
    '''
    gets reference-file and saves chromosome_names and sequences as dictonary
    returns dictonary
    '''
    chromosome_sequences = dict()
    chromosome_sequence = []
    chr_name = None
    saved = True
    with open(reference, 'r') as reference_file:
        line = reference_file.readline().split('\n')[0]
        while line:
            if '>' in line:
                if not saved:
                    chromosome_sequences[chr_name] = ''.join(
                        chromosome_sequence)
                    chromosome_sequence = []
                chr_name = line[1:]
                saved = False
            else:
                chromosome_sequence.append(line)
            line = reference_file.readline().split('\n')[0]
    chromosome_sequences[chr_name] = ''.join(chromosome_sequence)
    return chromosome_sequences


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'referencefile', help='file containing chromosome sequences')
    parser.add_argument(
        'samfile', help='samfile containing reads sorted by position on reference')
    parser.add_argument(
        'geneanotationfile', help='file containing information about coding regions and genes on chromosomes')
    parser.add_argument(
        'output', help='Saved files start with output_identifier', type=str)
    return parser
if __name__ == "__main__":
    main()
