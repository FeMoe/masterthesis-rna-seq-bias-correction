Masterarbeit: 

Thema: Bias-Korrekturverfahren für Genexpressionsdaten aus RNA-Seq

Anforderung:

* Python 3.4.2
* h5py 2.31
* Cython 0.21
* matplotlib 1.4.2
* numpy 1.9.1

Installation:
```
#!python setup.py build_ext --inplace

python setup.py build_ext --inplace
```


Notwendige Vorverarbeitung:

1. read_preprocessing.py: Führt die Vorverarbeitung von Reads im SAM-Format durch und gibt die für die Kontextzählung notwendigen Informationen in der Kommandozeile aus
2. context_counter.py: Führt die Zählung von Kontexten durch und speichert diese im hdf5-Format

Verzerrungscharakterisierung:

* nukleotide_distribution_drawer.py: Zeichnet die relativen Nukleotidhäufigkeiten aller Reads einer SAM-Datei.
* context_probability_plotter.py: Berechnet log-Wahrscheinlichkeiten mit denen ein Read mit einem bestimmten Kontext anfängt und zeichnet diese.
* g_test.py: Berechnet G-Werte gibt diese als Histogramm aus. Optional lassen sich Gen-IDs in durch Anwenderdefiniertem G-Wert-Intervall als pkl-Datei ausgeben (Voraussetzung für context_coverage.py)
* context_coverage.py: Berechnet überrepräsentierte Kontexte und gibt diese in der Kommandzeile aus

Verzerrungskorrektur:


* gene_affinity_processing.py: Berechnet Affinitätswerte für alle Gene (Voraussetzung für gene_expression_determination.py)
* gene_expression_determination.py: Ermittelt wie G-Werte für eine Liste von Datensätzen die Genexpressionswerte. Speichert in hdf5-Datei drei Tabellen.

* * Tabelle 1: Enthält für jedes Gen aus jedem Datensatz die Anzahl der gemappten Reads
* * Tabelle 2: Enthält für jedes Gen aus jedem Datensatz die anhand der Genaffinität geschätzten Expressionswerte 
* * Tabelle 3: Enthält für jedes Gen aus jedem Datensatz die anhand der Genlänge geschätzten Expressionswerte
* gene_expression_plotter.py: Plottet die Expressionswert von je zwei Datensätzen